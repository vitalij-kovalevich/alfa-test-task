import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAppsIfNeeded } from '../redux/effects';
import { getLoading, getFormData } from '../redux/selector';
import Spin from 'arui-feather/spin';
import Heading from 'arui-feather/heading';
import Form from './form/form';
import './App.css';

class App extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchAppsIfNeeded());
  }

  render() {
    const { isFetching, apps } = this.props;
    console.log(apps);
    return (
      <div class="wrapper">
        {isFetching && apps === null && 
          <Heading size='xl'> 
            <Spin
                size='xl'
                visible={ true }
            />
            Loading...
          </Heading>
        }
        {!isFetching && apps === null  && <Heading size='xl'>Empty.</Heading>}
        {
          apps !== null && <Form title={apps.title} fields={apps.fields} />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  const isFetching = getLoading(state);
  const apps = getFormData(state);
  return {
    isFetching,
    apps
  };
}

export default connect(mapStateToProps)(App);
