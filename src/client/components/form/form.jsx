import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  FormBuilder,
  FieldGroup,
  FieldControl,
  Validators,
} from "react-reactive-form";
import TextInput from './input';
import OptionInput from './option';
import NumbericInput from './numeric';
import { updateUserApi, cancelUpdateApi } from '../../redux/effects';
import { getLoading, getResult } from '../../redux/selector';
import FormAlfa from 'arui-feather/form';
import FormField from 'arui-feather/form-field';
import Button from 'arui-feather/button';
import Spin from 'arui-feather/spin';

class Form extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    this.cancle = {};
    this.state = {
      fields: {
        LIST : OptionInput,
        TEXT : TextInput,
        NUMERIC: NumbericInput,
      },
      dispatch,
      result : null
    };
    const fields = {};
    this.size = 'm';
    this.props.fields.map(field => {
      fields[field.name] = [(typeof field.values === 'array') ? field.values[0].text :  field.values, Validators.required];
    });

    this.loginForm = FormBuilder.group(fields);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.cancel = this.cancel.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    this.state.dispatch(updateUserApi(this.loginForm.value, this.cancle));
  }
  cancel() {
    this.state.dispatch(cancelUpdateApi(this.cancle));
  }
  render() {
    const { isFetching, result } = this.props
    return (
      <div class="form">
        <h2>{this.props.title}</h2>
        <FieldGroup
          control={this.loginForm}
          render={({ get, invalid }) => (
            <FormAlfa onSubmit={this.handleSubmit}>
              {
                this.props.fields.map(field => 
                  <FormField size={ this.size }>
                  <FieldControl
                    name={field.name}
                    render={this.state.fields[field.type]}
                    meta={{ label: field.title, options: field.values, size: this.size}}
                  />
                  </FormField>
                )
              }
              <Button view='extra' disabled={invalid} type='submit'>Submit</Button>
            </FormAlfa>
          )}
        />
      {
        isFetching && <div><Spin
          size='xl'
          visible={ true }
        />Loading... <Button view='extra' onClick={this.cancel}  >Cancel</Button></div>
      }
      {
        <div>{result}</div>
      }
      </div>
    );
  }

}

function mapStateToProps(state) {
  const result = getResult(state);
  const isFetching = getLoading(state);
  return {
    isFetching,
    result
  }
}
export default connect(mapStateToProps)(Form)
