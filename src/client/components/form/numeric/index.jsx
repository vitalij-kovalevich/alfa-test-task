import React from 'react';
import Input from 'arui-feather/input';
import Label from 'arui-feather/label';

const NumbericInput = ({ handler, touched, hasError, meta }) => (
  <div class="wrapper-item">
    <Label size={ meta.size } isNoWrap={ true }>{meta.label}</Label>
    <Input size={ meta.size } placeholder='Введите что-нибудь' {...handler()} type='number' />
    <span>
        {touched
        && hasError("required")
        && `${meta.label} is required`}
    </span>
  </div>  
);

export default NumbericInput;