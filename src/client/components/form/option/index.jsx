import React from 'react';
import Select from 'arui-feather/select';
import Label from 'arui-feather/label';

const OptionInput = ({ handler, touched, hasError, meta }) => (
  <div class="wrapper-item">
    <Label size={ meta.size } isNoWrap={ true }>{meta.label}</Label>
    <Select
        size={ meta.size }
        mode='radio'
        options={ meta.options }
        {...handler()}
    />
  </div>  
);

export default OptionInput;