export const REQUEST_APPS = 'REQUEST_APPS';
export const RECEIVE_APPS = 'RECEIVE_APPS';

export const UPDATE_USER = '[User] update user';
export const CANCEL_UPDATE_USER = '[User] cancel update user';
export const SUCCESS_UPDATE_USER = '[User] success update user';
export const FAIL_UPDATE_USER = '[User] fail update user'

export function requestApps() {
  return {
    type: REQUEST_APPS
  }
}
export function updateUser(data, id) {
  return {
    type: UPDATE_USER,
    data,
    id
  }
}
export function cancelUpdateUser(id) {
  return {
    type: CANCEL_UPDATE_USER,
    id
  }
}
export function successUpdateUser(data) {
  return {
    type: SUCCESS_UPDATE_USER,
    data : data
  }
}
export function failUpdateUser() {
  return {
    type: FAIL_UPDATE_USER
  }
}

export function receiveApps(json) {
  return {
    type: RECEIVE_APPS,
    apps: json
  }
}
