import axios from 'axios';
import axiosCancel from 'axios-cancel';
import {
  requestApps,
  updateUser,
  cancelUpdateUser,
  successUpdateUser,
  failUpdateUser,
  receiveApps
} from './../actions';

axiosCancel(axios, {
  debug: false // default
});

function fetchApps() {
  return dispatch => {
    dispatch(requestApps())
    return axios.post(`/getDataForm`)
      .then(response => response.data)
      .then(json => dispatch(receiveApps(json)))
  }
}
function fetchUpdateUser(data, cancel) {
  return dispatch => {
    dispatch(updateUser());
    const CancelToken = axios.CancelToken;
    cancel.source = CancelToken.source();
    axios.put(`/api/user`, data, {
      cancelToken: cancel.source.token
    })
      .then(response => response.data)
      .then(json => dispatch(successUpdateUser(json)))
      .catch(err => {
        if (axios.isCancel(thrown)) {
         
        } else {
          return dispatch(failUpdateUser());
        }
      })
  }
}

function shouldFetchApps(state) {
  const apps = state.apps
  if (apps === null) {
    return true
  } else if (state.isFetching) {
    return false
  }
}

export function fetchAppsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchApps(getState())) {
      return dispatch(fetchApps())
    }
  }
}

export function updateUserApi(data, id) {
  for (let key in data) {
    if (data[key].lenght) {
       data[key] = data[key][0];
    }
  }
  return (dispatch, getState) => {
    return dispatch(fetchUpdateUser(data, id))
  }
}

export function cancelUpdateApi(cancel){
  return (dispatch) => {
    cancel.source.cancel();
    return dispatch(cancelUpdateUser())
  }
}
