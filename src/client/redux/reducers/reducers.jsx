import {
  REQUEST_APPS,
  RECEIVE_APPS,
  UPDATE_USER,
  SUCCESS_UPDATE_USER,
  FAIL_UPDATE_USER,
  CANCEL_UPDATE_USER
} from '../actions';

const initial_state = { 
  isFetching: false,
  apps: [],
  loading: false,
  error : false,
  result: null
};

function reducer(state = initial_state, action) {
  switch (action.type) {
    case REQUEST_APPS:
      return {...state,
        isFetching: true
      };
    case RECEIVE_APPS:
      return {...state,
        isFetching: false,
        apps: action.apps
      };
    case UPDATE_USER:
      return {...state, 
        isFetching: true
      };
    case SUCCESS_UPDATE_USER:
      return {...state, 
        isFetching: false,
        error: false,
        result: action.data.result
      };
    case FAIL_UPDATE_USER:
      return {...state, 
        isFetching: false,
        error: true,
        result: null
      };
    case CANCEL_UPDATE_USER:
      return {...state, 
        isFetching: false,
        error: false,
        result: null
      };
    default:
      return state;
  }
}

export default reducer;
