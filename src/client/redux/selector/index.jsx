
export const getFormData = (state) => state.apps;
export const getResult = (state) => state.result;
export const getLoading = (state) => state.isFetching;