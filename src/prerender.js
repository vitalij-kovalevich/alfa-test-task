import React from 'react'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import configureStore from './client/redux/configureStore'
import App from './client/components/App'

export default function render (initialState) {
  const store = configureStore(initialState)

  const content = renderToString(
    <Provider store={store} >
       <App />
    </Provider>
  );

  const preloadedState = store.getState()
  return {content, preloadedState};
}