import express from 'express';
import Html from './template';
import rp from 'request-promise';
import render from './prerender';
import bodyParser from 'body-parser';

const port = 3000;
const server = express();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.use(express.static('dist'));
server.put('/api/user', (req, res) => setTimeout(async() => {
  const options = {
    method: 'POST',
    uri: 'http://test.clevertec.ru/tt/data',
    body: {
      form : req.body
    },
    json: true
  };
  const dataForm = await rp.post(options);
  res.json(dataForm)
}, 1000));

server.get('/', (req, res) => {
  const initialState = {
    isFetching: false,
    apps: null,
  }
  const title = 'Alfa';
  const { preloadedState, content } = render(initialState);
  
  res.send(
    Html({
      content,
      title,
      preloadedState
    })
  );
});

server.post('/getDataForm', async (req, res) => {
  const options = {
    method: 'POST',
    uri: 'http://test.clevertec.ru/tt/meta',
    json: true
  };
  const dataForm = convertFormData(await rp.post(options));

  res.send(dataForm);
});

server.listen(port);
console.log(`Serving at http://localhost:${port}`);

function convertFormData(dataForm) {
  dataForm.fields.forEach(field => {
    if (field.values) {
      field.values = convertToFormat(field.values)
    }
  })
  return dataForm;
}

function convertToFormat(values) {
  const options = [];

  for (let key in values) {
    options.push({ value: key, text: values[key] });
  }

  return options;
}