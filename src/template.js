const Html = ({ content, title, preloadedState }) => `
  <!DOCTYPE html>
  <html>
    <head>
      <title>${title}</title>
      <link href="./static/main.css" rel="stylesheet">
    </head>
    <body style="margin:0">
      <div id="react-app">${content}</div>
      <script>
        window.__STATE__ = ${JSON.stringify(preloadedState)}
      </script>
      <script src="./static/bundle.js"></script>
    </body>
  </html>
`;

module.exports = Html;